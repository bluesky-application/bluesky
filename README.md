![picture](https://i.imgur.com/mj675M8.png)

A open source BlueSky application for all platforms to use.

&nbsp;&nbsp;&nbsp;&nbsp;

  You can install Bluesky from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/bluesky/)

 ### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/bluesky/application/-/releases)

 ### Author
  * Corey Bruce
